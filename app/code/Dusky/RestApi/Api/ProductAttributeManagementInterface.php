<?php

namespace Dusky\RestApi\Api;
interface ProductAttributeManagementInterface
{
    /**
     * @param string $code
     * @param string $label
     * @return bool
     */
    public function saveAttribute($code, $label);
}
