<?php

namespace Dusky\RestApi\Model\Api;

use Dusky\RestApi\Api\ApiLogger;
use Dusky\RestApi\Api\ProductAttributeManagementInterface;
use Exception;
use Magento\Catalog\Model\Product;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Webapi\Rest\Response;


class ProductAttributeManagement implements ProductAttributeManagementInterface
{
    protected $eavSetupFactory;
    protected $logger;
    protected $response;


    public function __construct(EavSetupFactory $eavSetupFactory, ApiLogger $apiLogger,Response $response)
    {
        $this->eavSetupFactory = $eavSetupFactory;
        $this->logger = $apiLogger;
        $this->response = $response;
    }

    public function saveAttribute($code, $label)
    {
        $eavSetup = $this->eavSetupFactory->create();

        try {
            $eavSetup->addAttribute(Product::ENTITY, $code,
                [
                    'type' => 'text',
                    'backend' => '',
                    'frontend' => '',
                    'label' => $label,
                    'input' => 'text',
                    'visible' => true,
                    'required' => false,
                    'user_defined' => false,
                    'default' => '',
                    'searchable' => false,
                    'filterable' => false,
                    'comparable' => false,
                    'visible_on_front' => false,
                    'used_in_product_listing' => true,
                    'is_used_in_grid' => false,
                    'is_visible_in_grid' => false,
                    'is_filterable_in_grid' => false,
                    'unique' => false,
                    'apply_to' => ''
                ]
            );

            $this->response->setHeader('Content-Type', 'application/json', true)
                ->setBody('{"success":true}')
                ->sendResponse();
            $this->logger->info('Attribute saved. SKU:  ' . $code . ', value: ' . $label);

        } catch (Exception $e) {
            $this->response->setHeader('Content-Type', 'application/json', true)
                ->setBody('{"error":"Internal server error.","message:"' . $e->getMessage() . '"}')
                ->setHttpResponseCode(500)
                ->sendResponse();
            $this->logger->error($e->getMessage());
        }
    }
}
