<?php

namespace Dusky\RestApi\Api;

use Monolog\Logger;
use Magento\Framework\Logger\Handler\Base as BaseHandler;

class ApiHandler extends BaseHandler
{
    /**
     * Logging level
     * @var int
     */
    protected $loggerType = Logger::DEBUG;

    /**
     * @var string File name
     */
    protected $fileName = '/var/log/api_custom_logger.log';
}
